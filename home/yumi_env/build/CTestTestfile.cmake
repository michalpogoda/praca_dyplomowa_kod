# CMake generated Testfile for 
# Source directory: /home/yumi_env/src
# Build directory: /home/yumi_env/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(abb/abb)
subdirs(abb/abb_irb2400_moveit_config)
subdirs(abb/abb_irb6640_moveit_config)
subdirs(industrial_core/industrial_core)
subdirs(industrial_core/industrial_deprecated)
subdirs(industrial_core/industrial_msgs)
subdirs(abb/abb_irb2400_support)
subdirs(abb/abb_irb5400_support)
subdirs(abb/abb_irb6600_support)
subdirs(abb/abb_irb6640_support)
subdirs(industrial_core/simple_message)
subdirs(yumi/gazebo_mimic)
subdirs(industrial_core/industrial_utils)
subdirs(industrial_core/industrial_robot_client)
subdirs(abb/abb_driver)
subdirs(industrial_core/industrial_robot_simulator)
subdirs(abb/abb_irb2400_moveit_plugins)
subdirs(industrial_core/industrial_trajectory_filters)
subdirs(yumi/yumi_cameras)
subdirs(yumi/yumi_control)
subdirs(yumi/yumi_description)
subdirs(yumi/yumi_hw)
subdirs(yumi/yumi_moveit_config)
subdirs(yumi/yumi_support)
subdirs(yumi/yumi_launch)
subdirs(yumi/yumi_test_controllers)
