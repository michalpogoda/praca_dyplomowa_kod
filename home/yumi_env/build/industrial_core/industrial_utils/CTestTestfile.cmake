# CMake generated Testfile for 
# Source directory: /home/yumi_env/src/industrial_core/industrial_utils
# Build directory: /home/yumi_env/build/industrial_core/industrial_utils
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_industrial_utils_gtest_utest_inds_utils "/home/yumi_env/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/yumi_env/build/test_results/industrial_utils/gtest-utest_inds_utils.xml" "--return-code" "/home/yumi_env/devel/lib/industrial_utils/utest_inds_utils --gtest_output=xml:/home/yumi_env/build/test_results/industrial_utils/gtest-utest_inds_utils.xml")
