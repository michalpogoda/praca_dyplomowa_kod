# CMake generated Testfile for 
# Source directory: /home/yumi_env/src/industrial_core/simple_message
# Build directory: /home/yumi_env/build/industrial_core/simple_message
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_simple_message_gtest_utest "/home/yumi_env/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/yumi_env/build/test_results/simple_message/gtest-utest.xml" "--return-code" "/home/yumi_env/devel/lib/simple_message/utest --gtest_output=xml:/home/yumi_env/build/test_results/simple_message/gtest-utest.xml")
add_test(_ctest_simple_message_gtest_utest_byte_swapping "/home/yumi_env/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/yumi_env/build/test_results/simple_message/gtest-utest_byte_swapping.xml" "--return-code" "/home/yumi_env/devel/lib/simple_message/utest_byte_swapping --gtest_output=xml:/home/yumi_env/build/test_results/simple_message/gtest-utest_byte_swapping.xml")
add_test(_ctest_simple_message_gtest_utest_float64 "/home/yumi_env/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/yumi_env/build/test_results/simple_message/gtest-utest_float64.xml" "--return-code" "/home/yumi_env/devel/lib/simple_message/utest_float64 --gtest_output=xml:/home/yumi_env/build/test_results/simple_message/gtest-utest_float64.xml")
add_test(_ctest_simple_message_gtest_utest_udp "/home/yumi_env/build/catkin_generated/env_cached.sh" "/usr/bin/python" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/yumi_env/build/test_results/simple_message/gtest-utest_udp.xml" "--return-code" "/home/yumi_env/devel/lib/simple_message/utest_udp --gtest_output=xml:/home/yumi_env/build/test_results/simple_message/gtest-utest_udp.xml")
