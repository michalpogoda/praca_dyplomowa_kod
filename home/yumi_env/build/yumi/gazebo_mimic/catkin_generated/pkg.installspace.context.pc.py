# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/yumi_env/install/include".split(';') if "/home/yumi_env/install/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lgazebo_mimic_plugin".split(';') if "-lgazebo_mimic_plugin" != "" else []
PROJECT_NAME = "gazebo_mimic"
PROJECT_SPACE_DIR = "/home/yumi_env/install"
PROJECT_VERSION = "0.0.4"
